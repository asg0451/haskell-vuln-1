# what

simple example of how to do things unsafely in haskell. this is like the worst haskell code ive ever written, and it usually doesnt look like this i promise. but we're trying to be nasty here so stuff will break.

# how

## setup

install stack (could be called haskell-stack on your system)
run `stack setup` in this directory
run `stack build` in this directory

## run

the executable can be found with `echo $(stack path --dist-dir)/build/vuln1/vuln1`

theres a symlink to that in the project root, but idk if it'll still work after gittery
