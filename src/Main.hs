{-# LANGUAGE MultiWayIf          #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import           System.Environment

import           Control.Monad
import           Data.Vector.Primitive.Mutable as M
import           Text.Printf

import           Data.Int                      (Int32, Int64)

-- for function name clashes
import           Prelude                       as P

main :: IO ()
main = do
  as <- getArgs
  if | P.null as -> readOOB
     | head as == "input" -> inputArbSize
     | head as == "oob"   -> readOOB
     | otherwise -> readOOB


inputArbSize :: IO ()
inputArbSize = do
  (iov :: M.IOVector Char) <- M.new 16 -- 16 long char array

  inp <- getContents
  P.length inp `seq` return () -- force strict input

  forM_ (zip [0 .. ] inp) $ \(n,c) -> do
    putStrLn $ "writing " ++ show c ++ " at " ++ show n
    unsafeWrite iov n c

  putStrLn "printing array"
  forM_ (zip [0 .. ] inp) $ \(n,_) -> do
    c <- unsafeRead iov n
    putStrLn $ "reading " ++ show c ++ " at " ++ show n

readOOB :: IO ()
readOOB = do
  (iov :: M.IOVector Int32) <- M.new 64 -- new mutable array of ints of length 64
  forM_ [64..] $ \n -> -- ooooh scary
     printf "0x%Lx\n" =<< M.unsafeRead iov n
